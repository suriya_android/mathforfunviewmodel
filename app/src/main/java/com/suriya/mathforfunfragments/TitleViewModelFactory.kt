package com.suriya.mathforfunfragments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class TitleViewModelFactory(private val sumCorrect:Int,private val sumIncorrect:Int):ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(TitleViewModel::class.java)){
            return TitleViewModel(sumCorrect,sumIncorrect) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}