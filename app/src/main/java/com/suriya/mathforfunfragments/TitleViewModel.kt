package com.suriya.mathforfunfragments

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TitleViewModel(sumCorrect: Int, sumIncorrect: Int) : ViewModel() {

    private val _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct

    private val _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect

    private val _eventStartGame = MutableLiveData<Int>()
    val eventStatGame: LiveData<Int>
        get() = _eventStartGame

    fun onClickGamePlus() {
        _eventStartGame.value = 1
        Log.i("plus","plusGame")
    }

    fun onClickGameMin() {
        _eventStartGame.value = 2
    }

    fun onClickGameMul() {
        _eventStartGame.value = 3
    }

    fun onEventCompleted() {
        _eventStartGame.value = 0
    }


    init {
        _correct.value = sumCorrect
        _incorrect.value = sumIncorrect
    }
}