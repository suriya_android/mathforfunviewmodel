package com.suriya.mathforfunfragments

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.suriya.mathforfunfragments.databinding.FragmentTitleBinding


private lateinit var viewModel: TitleViewModel
private lateinit var viewModelFactory: TitleViewModelFactory
private lateinit var binding: FragmentTitleBinding

class TitleFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_title, container, false)
        // Do
        setHasOptionsMenu(true)
        viewModelFactory = TitleViewModelFactory(
            TitleFragmentArgs.fromBundle(requireArguments()).gloCorrect,
            TitleFragmentArgs.fromBundle(requireArguments()).gloIncorrect
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(TitleViewModel::class.java)
        viewModel.eventStatGame.observe(viewLifecycleOwner, Observer { startGame ->
            when (startGame) {
                1 -> {
                    val action = TitleFragmentDirections.actionTitleFragment2ToPlusGameFragment()
                    action.plusCorrect = viewModel.correct.value!!
                    action.plusIncorrect = viewModel.incorrect.value!!
                    view?.findNavController()?.navigate(action)
                    viewModel.onEventCompleted()
                }
                2 -> {
                    val action = TitleFragmentDirections.actionTitleFragment2ToMinusGameFragment()
                    action.minusCorrect = viewModel.correct.value!!
                    action.minusIncorrect = viewModel.incorrect.value!!
                    view?.findNavController()?.navigate(action)
                    viewModel.onEventCompleted()
                }
                3 -> {
                    val action =
                        TitleFragmentDirections.actionTitleFragment2ToMultipleGameFragment()
                    action.mulCorrect = viewModel.correct.value!!
                    action.mulIncorrect = viewModel.incorrect.value!!
                    view?.findNavController()?.navigate(action)
                    viewModel.onEventCompleted()
                }
            }
        })
        binding.titleViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.option_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(
            item,
            requireView().findNavController()
        ) || super.onOptionsItemSelected(item)
    }
}