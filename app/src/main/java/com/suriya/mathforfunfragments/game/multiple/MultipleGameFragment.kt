package com.suriya.mathforfunfragments.game.multiple

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.suriya.mathforfunfragments.game.multiple.MultipleGameFragmentArgs
import com.suriya.mathforfunfragments.game.multiple.MultipleGameFragmentDirections
import com.suriya.mathforfunfragments.R
import com.suriya.mathforfunfragments.databinding.FragmentMultipleGameBinding
import com.suriya.mathforfunfragments.game.minus.MinusGameFragmentDirections
import com.suriya.mathforfunfragments.game.plus.PlusViewModel
import kotlin.random.Random


private lateinit var binding:FragmentMultipleGameBinding
private lateinit var viewModel: MultipleViewModel
class MultipleGameFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_multiple_game, container, false)
        viewModel = ViewModelProvider(this).get(MultipleViewModel::class.java)
        val args = MultipleGameFragmentArgs.fromBundle(requireArguments())
        viewModel.setValue(args.mulCorrect,args.mulIncorrect)
        onBackPressed()
        binding.mulViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    private fun onBackPressed(){
        requireActivity().onBackPressedDispatcher.addCallback(this){
            val action = MultipleGameFragmentDirections.actionMultipleGameFragmentToTitleFragment2()
            action.gloCorrect = viewModel.correct.value?.plus(viewModel.getCorrect.value!!) ?: 0
            action.gloIncorrect = viewModel.incorrect.value?.plus(viewModel.getIncorrect.value!!)?:0
            Log.e("Test",action.gloCorrect.toString())
            view?.findNavController()?.navigate(action)
        }
    }

}