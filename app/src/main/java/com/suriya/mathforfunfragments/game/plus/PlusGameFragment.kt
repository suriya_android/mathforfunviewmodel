package com.suriya.mathforfunfragments.game.plus


import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.suriya.mathforfunfragments.game.plus.PlusGameFragmentArgs
import com.suriya.mathforfunfragments.game.plus.PlusGameFragmentDirections
import com.suriya.mathforfunfragments.R
import com.suriya.mathforfunfragments.TitleFragment
import com.suriya.mathforfunfragments.TitleFragmentArgs
import com.suriya.mathforfunfragments.databinding.FragmentPlusGameBinding
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class PlusGameFragment : Fragment() {

    private lateinit var binding: FragmentPlusGameBinding
    private lateinit var viewModel: PlusViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_plus_game,
            container,
            false
        )
        onBackPressed()
        viewModel = ViewModelProvider(this).get(PlusViewModel::class.java)
        val args = PlusGameFragmentArgs.fromBundle(requireArguments())
        viewModel.setValue(args.plusCorrect,args.plusIncorrect)

        Log.e("Log","${args.plusCorrect} + ${args.plusIncorrect}")
        binding.gameViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    private fun onBackPressed() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            val action = PlusGameFragmentDirections.actionPlusGameFragmentToTitleFragment2()
            action.gloCorrect = viewModel.correct.value?.plus(viewModel.getCorrect.value!!) ?: 0
            action.gloIncorrect =
                viewModel.incorrect.value?.plus(viewModel.getIncorrect.value!!) ?: 0
            Log.e("Test", action.gloCorrect.toString())
            view?.findNavController()?.navigate(action)
        }
    }

}