package com.suriya.mathforfunfragments.game.minus

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class MinusViewModel:ViewModel(){
    //    Declare
    private var _num1 = MutableLiveData<Int>();
    val num1: LiveData<Int>
        get() = _num1

    private var _num2 = MutableLiveData<Int>();
    val num2: LiveData<Int>
        get() = _num2

    private var _sum = MutableLiveData<Int>();
    val sum: LiveData<Int>
        get() = _sum

    private var _result = MutableLiveData<String>();
    val result: LiveData<String>
        get() = _result

    private var _correct = MutableLiveData<Int>();
    val correct: LiveData<Int>
        get() = _correct

    private var _incorrect = MutableLiveData<Int>();
    val incorrect: LiveData<Int>
        get() = _incorrect


    private var _btnValue1 = MutableLiveData<Int>();
    val btnValue1: LiveData<Int>
        get() = _btnValue1

    private var _btnValue2 = MutableLiveData<Int>();
    val btnValue2: LiveData<Int>
        get() = _btnValue2

    private var _btnValue3 = MutableLiveData<Int>();
    val btnValue3: LiveData<Int>
        get() = _btnValue3

    private var _getCorrect = MutableLiveData<Int>();
    val getCorrect: LiveData<Int>
        get() = _getCorrect

    private var _getInCorrect = MutableLiveData<Int>();
    val getIncorrect: LiveData<Int>
        get() = _getInCorrect


    private var i = 0

    init {
        _num1.value = 0
        _num2.value = 0
        _sum.value = 0
        _result.value = ""
        _correct.value = 0
        _incorrect.value = 0
        _btnValue1.value = 0
        _btnValue2.value = 0
        _btnValue3.value = 0
        _getCorrect.value = 0
        _getInCorrect.value = 0
        randomQuestion()
        initialButtonAnswer()
    }

    fun randomQuestion() {
        _num1.value = Random.nextInt(1, 5)
        _num2.value = Random.nextInt(1, 5)
        _sum.value = _num1.value!! - _num2.value!!
    }

    fun answerName(answer:String){
        _result.value = answer
    }

    fun isCorrect(){
        _correct.value = (correct.value)?.plus(1)
    }

    fun isIncorrect(){
        _incorrect.value = (incorrect.value)?.plus(1)
    }

    fun setValue(value1:Int,value2:Int){
        _getCorrect.value = value1
        _getInCorrect.value = value2
    }



    fun checkAnswer(value: Int) {
        if (value == _sum.value) {
            answerName("ถูกต้อง")
            isCorrect()
        } else {
            answerName("ไม่ถูกต้อง")
            isIncorrect()
        }
        Handler().postDelayed({
            randomQuestion()
            initialButtonAnswer()
        }, 250)
    }



    fun initialButtonAnswer() {
        i = Random.nextInt(1, 3)
        when (i) {
            1 -> {
                _btnValue1.value = _sum.value
                _btnValue2.value = _sum.value?.plus(1)
                _btnValue3.value = _sum.value?.plus(2)
            }
            2 -> {
                _btnValue2.value = _sum.value
                _btnValue1.value = _sum.value?.minus(1)
                _btnValue3.value = _sum.value?.plus(1)
            }
            3 -> {
                _btnValue3.value = _sum.value
                _btnValue1.value = _sum.value?.minus(1)
                _btnValue2.value = _sum.value?.minus(2)
            }
        }
    }
}